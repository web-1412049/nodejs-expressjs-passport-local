var express = require('express');
var router = express.Router();
var productController = require("../controllers/productController");

/* GET products list page.
* good
*/
router.get('/', productController.list);

router.get('/new', productController.new);

router.get('/edit', productController.find);

router.post('/add', productController.insert);

router.get('/del', productController.delete);

module.exports = router;
