var Product = require('../models/product');

exports.list = function (req, res, next) {
    Product.find({}, function (err, lstproduct) {
        if (err) {
            console.log(err);
            res.render('products', {title: 'Products Manager', smenu: "products", productlst: [], user: req.user});
        }
        else
            res.render('products', {title: "Product manager", smenu: 'products', productlst: lstproduct, user: req.user});
    });
};

exports.find = function (req, res, next) {
    Product.findById(req.query.id, function (err, doc) {
        if (err) {
            console.log(err);
            res.redirect('/products');
        } else {
            res.render('item', {title: 'Edit product', smenu: "products", p: doc, user: req.user});
        }
    });
};

exports.insert = function (req, res, next) {
    if (req.body.id === "auto") {
        var product_instance = new Product({
            name: req.body.name,
            category: req.body.category,
            cost: req.body.cost
        });
        product_instance.save(function (err) {
            if (err) {
                console.log(err);
            }
            res.redirect('/products');
        });
    }
    else {
        Product.findById(req.body.id, function (err, doc) {
            if (err) {
                console.log(err);
                res.redirect('/products');
                return;
            }
            doc.name = req.body.name;
            doc.cost = req.body.cost;
            doc.category = req.body.category;
            doc.save(function (err) {
                if (err) {
                    console.log(err);
                }
                res.redirect('/products');
            })
        });
    }
};

exports.delete = function (req, res, next) {
    Product.remove({_id: req.query.id}, function (err) {
        if (err) {
            console.log(err);
        }
        res.redirect('/products');
    });
};

exports.new = function (req, res, netx) {
    res.render('item', {title: 'Add new product', smenu: "products", p: {_id: "auto", name: "", cost: ""}, user: req.user});
}
