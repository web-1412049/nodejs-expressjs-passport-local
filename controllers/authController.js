var mongoose = require("mongoose");
var passport = require("passport");
var User = require("../models/user");

var userController = {};

// Restrict access to root page
userController.home = function(req, res) {
    res.render('index', {title: 'Dashboard', smenu: "home", user : req.user });
};

// Go to registration page
userController.register = function(req, res) {
    res.render('register');
};

// Post registration
userController.doRegister = function(req, res) {
    User.register(new User({ username : req.body.username, name: req.body.name }), req.body.password, function(err, user) {
        if (err) {
            return res.render('register', { user : user });
        }

        passport.authenticate('local')(req, res, function () {
            res.redirect('/');
        });
    });
};

// Go to login page
userController.login = function(req, res) {
    res.render('login');
};

// Post login
userController.doLogin = passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login' });

// logout
userController.logout = function(req, res) {
    req.logout();
    res.redirect('/login');
};

// Check user
userController.isAuthenticated = function (req, res, next) {
    if(!req.user){
        res.redirect('/login');
    } else {
        next();
    }
};

module.exports = userController;
