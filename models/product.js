var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ProductSchema = Schema(
    {
        name: String,
        category: String,
        cost: String
    }
);

module.exports = mongoose.model('Product', ProductSchema);
